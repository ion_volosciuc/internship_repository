let menuButton = document.querySelector("#menu-button");
let menu = document.querySelector("#main-menu");
let mainContent = document.querySelector("#main-content");
let footer = document.querySelector("#footer");
let opened = false;
// $("#menu-button").click(function() {
// 	$("#main-menu").slideToggle();
// });

// if (mainContent.scrollHeight < window.innerHeight)
// 	footer.classList.add("fixedFooter");

menuButton.addEventListener("click", (e) => {

	if (opened)
		menu.classList.remove("closed");
	else
		menu.classList.add("closed");

	opened = !opened;
})

window.addEventListener("resize", () => {
	if (window.innerWidth >= 1024){
		menu.style.height = "auto";
		menu.classList.remove("closed");
		opened = false;
	} else {
		menu.removeAttribute("style");
		if (opened == true)
			menu.classList.add("closed");
	}
	 
})