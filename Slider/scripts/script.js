var container = document.getElementById("slider-container");
var slider = document.getElementById("slider");
var arrows = document.createElement("div");
var leftArr = document.createElement("div");
var rightArr = document.createElement("div");
var slides = 3;
arrows.className = "arrows"
slider.style.left = "0px";
leftArr.innerHTML = "&#x2039";
leftArr.className = "arrow left-arrow";
rightArr.innerHTML = "&#x203A";
rightArr.className = "arrow right-arrow";

fetch('../images.json')
	.then(res => res.json())
	.then(res => console.log(res));


arrows.appendChild(leftArr);
arrows.appendChild(rightArr);
container.insertBefore(arrows, container.firstChild);

rightArr.addEventListener("click", function() {
	slide(-1);
})

leftArr.addEventListener("click", function() {
	slide(1);
})

function slide(x) {
	slider.style.left = parseInt(slider.style.left) + (800 * x) + "px";
	if (slider.style.left == "800px")
		slider.style.left = "-1600px";
	else if (slider.style.left == "-2400px")
		slider.style.left = "0px";
}

var list = document.createElement("ul");
list.className = "dots";
var dots = [];

for (let i = 0; i < 3; i++) {
	let li = document.createElement("li");
	list.appendChild(li);
	li.className = "dot";
	li.addEventListener("click", function() {
		slider.style.left = i * -800 + "px";
	})
}
container.appendChild(list);